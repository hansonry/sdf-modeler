local dir_src  = (...):gsub('%.[^%.]+$', '') .. "."
local dir_root = (...):gsub('%.src.[^%.]+$', '') .. "."
local cpml = require(dir_root .. "libs.cpml")
local vec3 = cpml.vec3
local mat4 = cpml.mat4


local SignedDistance = {}
SignedDistance.__index = SignedDistance

local SDF = {SignedDistance = SignedDistance}



function SignedDistance:new(distance, color)
   local sdf = {
      distance = distance,
      color = color
   }
   setmetatable(sdf, self)
   return sdf
end

function SignedDistance:__tostring()
   return string.format("SignedDist(%f, %s)", self.distance, self.color)
end

function SignedDistance:copy()
   return SignedDistance:new(self.distance, self.color)
end

function SignedDistance:union(sdv)
   if self.distance < sdv.distance then
      return self:copy()
   end
   return sdv:copy()
end

function SignedDistance:subtract(sdv)
   if -self.distance > sdv.distance then
      local copy = sdv:copy()
      copy.distance = -sdv.distance
      return copy
   end
   return self:copy()
end


function SDF.castRay(SDFunction, start, direction, epsilon, maxFartherJumps)
   maxFartherJumps = maxFartherJumps or 10
   epsilon = epsilon or 0.001
   direction = direction:normalize()
   local fartherJump = 0
   local point = start:clone()
   local prevSDV = nil
   local steps = 0
   local function ComputeDistanceFromStart()
      return (point - start):len()
   end
   local resultData = {
      hit = false,
      start = start,
      point = point,
      epsilon = epsilon,
      maxFartherJumps = maxFartherJumps,
      direction = direction,
   }
   while fartherJump < maxFartherJumps do
      
      local sdv = SDFunction(point)
      resultData.sdv = sdv
      --print("p", point, sdv.distance)
      if sdv.distance < epsilon then
         resultData.hit = true
         resultData.steps = steps
         resultData.fartherJump = fartherJump
         resultData.distance = ComputeDistanceFromStart()
         return resultData
      end
      if prevSDV ~= nil then
         if prevSDV.distance < sdv.distance then
            fartherJump = fartherJump + 1
         else
            fartherJump = 0
         end
      end
      
      prevSDV = sdv
      point.x = point.x + (direction.x * sdv.distance)
      point.y = point.y + (direction.y * sdv.distance)
      point.z = point.z + (direction.z * sdv.distance)
      steps = steps + 1
   end
   resultData.hit = false
   resultData.steps = steps
   resultData.fartherJump = fartherJump
   resultData.distance = ComputeDistanceFromStart()
   return resultData
end

function SDF.findSurface(SDFunction, start, normal, epsilon, maxSteps)
   epsilon = epsilon or 0.001
   maxSteps = maxSteps or 10
   local result = {
      SDFunction = SDFunction,
      start      = start,
      normal     = normal,
      epsilon    = epsilon,
      maxSteps   = maxSteps,
   }
   local steps = 0
   local point = start:clone()
   local closestPoint = start:clone()
   local closestSDV = nil
   for i = 1, maxSteps do
      local sdv = SDFunction(point)
      
      if (closestSDV == nil or 
          math.abs(sdv.distance) < math.abs(closestSDV.distance)) then
         closestSDV     = sdv
         closestPoint.x = point.x
         closestPoint.y = point.y
         closestPoint.z = point.z
      end
      if math.abs(sdv.distance) < epsilon then
         result.hit          = true
         result.point        = point
         result.sdv          = sdv
         result.steps        = steps
         result.closestPoint = closestPoint
         result.closestSDV   = closestSDV
         return result
      end
      
      point.x = point.x - (normal.x * sdv.distance)
      point.y = point.y - (normal.y * sdv.distance)
      point.z = point.z - (normal.z * sdv.distance)
      
      steps = steps + 1
   end
   result.hit          = false
   result.point        = point
   result.sdv          = sdv
   result.steps        = steps
   result.closestPoint = closestPoint
   result.closestSDV   = closestSDV
   return result
end

function SDF.computeNormal(SDFunction, position, radius)
   radius = radius or 0.001
   local p = vec3.new(0,0,0)
   local function computeOffset(dx, dy, dz)
      p.x = position.x + dx * radius
      p.y = position.y + dy * radius
      p.z = position.z + dz * radius
      return SDFunction(p)
   end
   local xp_sdv = computeOffset( 1,  0,  0)
   local xn_sdv = computeOffset(-1,  0,  0)
   local yp_sdv = computeOffset( 0,  1,  0)
   local yn_sdv = computeOffset( 0, -1,  0)
   local zp_sdv = computeOffset( 0,  0,  1)
   local zn_sdv = computeOffset( 0,  0, -1)
   local function makeNormalComp(p, n)
      return (p.distance - n.distance) / (radius * 2)
   end
   local notNorm = vec3.new(makeNormalComp(xp_sdv, xn_sdv),
                            makeNormalComp(yp_sdv, yn_sdv),
                            makeNormalComp(zp_sdv, zn_sdv)) 
   return notNorm
end

function SDF.translate(position, x, y, z)
   return vec3.new(position.x - x, position.y - y, position.z - z)
end

function SDF.rotateAxisAngle(position, axis, angle_rad)
   local matrix = mat4.from_angle_axis(-angle_rad, axis)
   return matrix * position
end

function SDF.makeSphere(position, radius, color)
   local dist = position:len() - radius
   return SignedDistance:new(dist, color)
end

local function vec3abs(v)
   return vec3.new(math.abs(v.x), math.abs(v.y), math.abs(v.z))
end

local function vec3max(vec, val )
   return vec3.new(math.max(vec.x, val), math.max(vec.y, val), math.max(vec.z, val))
end

function SDF.makeBox(position, size, color)
   local q = vec3abs(position) - size
   local dist = vec3max(q, 0):len() + math.min(math.max(q.x, math.max(q.y, q.z)), 0)
   return SignedDistance:new(dist, color)
end


function SDF.makeCylinder(position, height, radius, color)
   local cp = position:clone()
   cp.y = 0
   local x = cp:len() - radius
   local y = math.abs(position.y) - height
   local px = math.max(x, 0)
   local py = math.max(y, 0)
   local len = math.sqrt(px * px + py * py)
   local dist = math.min(math.max(x, y), 0) + len
   return SignedDistance:new(dist, color)
end



return SDF

