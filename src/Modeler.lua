local dir_src  = (...):gsub('%.[^%.]+$', '') .. "."
local dir_root = (...):gsub('%.src.[^%.]+$', '') .. "."
local cpml = require(dir_root .. "libs.cpml")
local vec3 = cpml.vec3
local mat4 = cpml.mat4
local SDF  = require(dir_src .. "SDF")
local Image = require(dir_src .. "Image")
local TimerSet = require(dir_src .. "TimerSet")


local function createRotationMatrixBetweenVectors(from, to)
   local function addMat(a, b)
      local t = {}
      for i = 1,16 do
         t[i] = a[i] + b[i]
      end
      return mat4.new(t)
   end
   local c = to:cross(from)
   local s2 = c:len2()
   local d = to:dot(from)
   if s2 < 0.001 then
      if d > 0 then
         local mat = mat4.identity()
         return mat
      else
         local goofy = vec3.new(0, 1, 0)
         if math.abs(goofy:dot(from)) > 0.9 then
            goofy = vec3.new(1, 0, 0)
         end
         local gc = from:cross(goofy)
         local mat = mat4.from_angle_axis(math.pi, gc)
         return mat
      end
   end
    
   local v = mat4.new({
       0,   -c.z,  c.y,
       c.z,  0,   -c.x,
      -c.y,  c.x,  0,
   })
   local scale = (1 - d) / s2
   local scaledSquare = mat4.new():scale((v * v), vec3.new(scale, scale, scale))
   local rotMat = addMat(addMat(v, scaledSquare), mat4.identity()) 
   rotMat[16] = 1
   return rotMat
end

local function createRotationMatrixBetweenTriangles(v1, v2, v3, vt1, vt2, vt3)
   local deltaPos1 = v2 - v1
   local deltaPos2 = v3 - v1
   local deltaUV1 = vt2 - vt1
   local deltaUV2 = vt3 - vt1
   local tNormal = deltaPos1:cross(deltaPos2):normalize()
   local vtNormal = deltaUV1:cross(deltaUV2):normalize()
   
   local normRot = createRotationMatrixBetweenVectors(tNormal, vtNormal)
   
   local deltaPos1Rot = normRot * deltaPos1
   
   local delta1Rot = createRotationMatrixBetweenVectors(deltaPos1Rot:normalize(), deltaUV1:normalize())
   
   local totalRot = mat4.new():scale(delta1Rot * normRot, vec3.new(1, 1, -1))

   return totalRot
   
end


local Mesh = {}
Mesh.__index = Mesh

function Mesh:new(name, makeColorImage, makeNormalImage)
   name = name or "unnamed"
   makeColorImage = makeColorImage or true
   makeNormalImage = makeNormalImage or true
   local mesh = {
      vertices = {},
      vertexLookup = {},
      triangles = {},
      UVs = {},
      name = name,
      makeColorImage = makeColorImage,
      makeNormalImage = makeNormalImage,
      objFilename      = name .. ".obj",
      materialFilename = name .. "_mat.mtl",
      colorImageName   = name .. "_color",
      normalImageName  = name .. "_normal",

      nextIndex = 1
   }
   
   setmetatable(mesh, self)
   return mesh
end

function Mesh:addTraingle(p1, p2, p3)
   local startIndex = self.nextIndex
   function addPoint(p)
      if self.vertexLookup[p] == nil then
         local index = self.nextIndex
         self.vertexLookup[p] = index
         self.vertices[index] = p
         self.nextIndex = self.nextIndex + 1
      end
      return self.vertexLookup[p]
   end
   
   local triangle = {
      v = {
         addPoint(p1),
         addPoint(p2),
         addPoint(p3)
      }
   }
   table.insert(self.triangles, triangle)
end

function Mesh:generateUVMap(sizeInPixels)
   sizeInPixels = sizeInPixels or 1024
   local uvIndex = 1
   local function addUVTo(uv)
      local index = uvIndex
      self.UVs[index] = uv
      uvIndex = uvIndex + 1
      return index
   end
   local px = 1 / sizeInPixels
   local hpx = px / 2
   local area = #self.triangles --/ 2
   local widthInTri = math.ceil(math.sqrt(area))
   local pxPerTri = math.floor(sizeInPixels / widthInTri)
   local unit =  pxPerTri / sizeInPixels
   local proto = { 
      {hpx, unit - hpx},
      {unit - hpx, hpx},
      {hpx, hpx},
   }
   local y = 0
   local x = 0
   local count = 0
   for i, tri in ipairs(self.triangles) do
      tri.vt = {}
      for k, uv in ipairs(proto) do
            table.insert(tri.vt, addUVTo(vec3.new(uv[1] + x, uv[2] + y, 0)))
      end
      x = x + unit
      count = count + 1
      if count % widthInTri == 0 then
         x = 0
         y = y + unit
      end
   end
end



function Mesh:renderMaps(size, SDFunction)
   local px = 1 / size
   local hpx = px / 2
   local function mapUVToWorldTriangle(tri, uv)
      -- compute Barycentric point
      local v0 = self.UVs[tri.vt[2]] - self.UVs[tri.vt[1]]
      local v1 = self.UVs[tri.vt[3]] - self.UVs[tri.vt[1]]
      local v2 = uv                  - self.UVs[tri.vt[1]]
      
      local d00 = v0:dot(v0)
      local d01 = v0:dot(v1)
      local d11 = v1:dot(v1)
      local d20 = v2:dot(v0)
      local d21 = v2:dot(v1)
      local denom = d00 * d11 - d01 * d01
      local v = (d11 * d20 - d01 * d21) / denom
      local w = (d00 * d21 - d01 * d20) / denom
      local u = 1 - v - w

      -- Compute world point from Barycentric (u, v, w)
      local newPoint = ((self.vertices[tri.v[1]] * u) + 
                        (self.vertices[tri.v[2]] * v) + 
                        (self.vertices[tri.v[3]] * w))

      return newPoint
      
   end
   local function signedArea(x1, y1, x2, y2, x3, y3)
      return ((x2 - x1) * (y3 - y1) - (y2 - y1) * (x3 - x1)) / 2
   end
   local function normToColor(v)
      return math.min(math.max((1 + v) / 2, 0), 1)
   end
   local colorImage = nil
   local normalImage = nil
   if self.makeColorImage then
      colorImage = Image:new(size, size)
   end
   if self.makeNormalImage then
      normalImage = Image:new(size, size)
   end
   for i, tri in ipairs(self.triangles) do
      assert(tri.vt, "Expected UV Map to be built by now")
      local imageCoords = {}
      local bbox = nil
      local uvCoords = {}
      local v1 = self.vertices[tri.v[2]] - self.vertices[tri.v[1]]
      local v2 = self.vertices[tri.v[3]] - self.vertices[tri.v[1]]
      local triangleNormal = v1:cross(v2)
      local canDoNormals = triangleNormal:len() > 0.00001
      local rotMatrix = nil
      if canDoNormals then
         triangleNormal = triangleNormal:normalize()
         rotMatrix = createRotationMatrixBetweenTriangles(
            self.vertices[tri.v[1]], self.vertices[tri.v[2]], self.vertices[tri.v[3]],
            self.UVs[tri.vt[1]],     self.UVs[tri.vt[2]],     self.UVs[tri.vt[3]])
      end
      -- convert to image coords and bound box
      for k, uv in ipairs(tri.vt) do
         uvCoords[k] = self.UVs[uv]
         imageCoords[k] = vec3.new(
            self.UVs[uv].x * colorImage.width,
            (1 - self.UVs[uv].y) * colorImage.height, 
            0
         )
         if bbox == nil then
            bbox = {imageCoords[k].x, imageCoords[k].y, imageCoords[k].x, imageCoords[k].y}
         else
            bbox[1] = math.min(bbox[1], imageCoords[k].x)
            bbox[2] = math.min(bbox[2], imageCoords[k].y)
            bbox[3] = math.max(bbox[3], imageCoords[k].x)
            bbox[4] = math.max(bbox[4], imageCoords[k].y)
         end
      end

      bbox[1] = math.max(bbox[1], 0)
      bbox[2] = math.max(bbox[2], 0)
      bbox[3] = math.min(bbox[3], colorImage.width - 1)
      bbox[4] = math.min(bbox[4], colorImage.height - 1)

      
      bbox[1] = math.floor(bbox[1])
      bbox[2] = math.floor(bbox[2])
      bbox[3] = math.ceil(bbox[3])
      bbox[4] = math.ceil(bbox[4])
      
      for y = bbox[2], bbox[4] do
         for x = bbox[1], bbox[3] do
            local uvOfPoint = vec3.new((x + 0.5) / size, 
                                       1 - ((y + 0.5) / size), 0)
            local function isInTri(x, y) 
               local sa1 = signedArea(uvCoords[1].x, uvCoords[1].y, uvCoords[2].x, uvCoords[2].y, x, y)
               local sa2 = signedArea(uvCoords[2].x, uvCoords[2].y, uvCoords[3].x, uvCoords[3].y, x, y)
               local sa3 = signedArea(uvCoords[3].x, uvCoords[3].y, uvCoords[1].x, uvCoords[1].y, x, y)
               return ((sa1 >= 0 and sa2 >= 0 and sa3 >= 0) or
                       (sa1 <= 0 and sa2 <= 0 and sa3 <= 0))
            end
            -- Checking -hpx, -hpx because of the slanted side of the triangle
            -- This will not work in all cases.
            if (isInTri(uvOfPoint.x, uvOfPoint.y) or 
                isInTri(uvOfPoint.x - hpx, uvOfPoint.y - hpx)) then
               local worldPoint = mapUVToWorldTriangle(tri, uvOfPoint)
               
               local ipx = x + 1 -- image pixel x
               local ipy = y + 1 -- image pixel y
               local surface = {}
               if canDoNormals then
                  local search = SDF.findSurface(SDFunction, worldPoint, triangleNormal)
                  if search.hit then
                     surface.point = search.point
                     surface.sdv   = search.sdv
                  else
                     surface.point = search.closestPoint
                     surface.sdv   = search.closestSDV
                  end
               else
                  surface.point = worldPoint
                  surface.sdv   = SDFunction(worldPoint)

               end
               -- Color Data
               if self.makeColorImage then
                  local sdv = surface.sdv
                  colorImage:set(ipx, ipy, sdv.color[1], sdv.color[2], sdv.color[3])
               end
   
               -- Normal Data
               if self.makeNormalImage then
                  if canDoNormals then
                     local normal = SDF.computeNormal(SDFunction, surface.point):normalize()
                     local tangent = (rotMatrix * normal):normalize()
                     
                     normalImage:set(ipx, ipy, normToColor(tangent.x), 
                                               normToColor(tangent.y), 
                                               normToColor(tangent.z))
                  else
                     normalImage:set(ipx, ipy, 0, 0, 1)
                  end
               end
            end
         end
      end
   end
   self.colorImage = colorImage
   self.normalImage = normalImage
end

function Mesh:writeObjModelFiles()
   local f = string.format
   local createMaterialFile = self.makeColorImage or self.makeNormalImage
   local materialName = "material"
   
   local fh = io.open(self.objFilename, "w")
   
   if createMaterialFile then
      fh:write(f("mtlib %s\n",  self.materialFilename))
      fh:write(f("usemtl %s\n", materialName))
   end
   
   for i, vertex in ipairs(self.vertices) do
      fh:write(f("v %f %f %f\n", vertex.x, vertex.y, vertex.z))
   end
   
   for i, uv in ipairs(self.UVs) do
      fh:write(f("vt %f %f\n", uv.x, uv.y))
   end
   
   for i, tri in ipairs(self.triangles) do
      if tri.vt == nil then
         fh:write(f("f %d %d %d\n", tri.v[1], tri.v[2], tri.v[3]))
      else
         fh:write(f("f %d/%d %d/%d %d/%d\n", tri.v[1], tri.vt[1], 
                                             tri.v[2], tri.vt[2],
                                             tri.v[3], tri.vt[3]))
      end
   end
   
   fh:close()
   
   if createMaterialFile then
      local imageExtention = ".png" --".ppm"
      local fh = io.open(self.materialFilename, "w")
      fh:write(f("newmtl %s\n", materialName))
      fh:write("Ka 1.000 1.000 1.000\n")
      fh:write("Kd 1.000 1.000 1.000\n")
      fh:write("Ks 0.000 0.000 0.000\n")
      fh:write("d 1.0\n")
      fh:write("illum 0\n")
      if self.makeColorImage then
         fh:write(f("map_Kd %s\n", self.colorImageName .. imageExtention))
      end
      if self.makeNormalImage then
         fh:write(f("norm %s\n", self.normalImageName .. imageExtention))
      end
      fh:close()
   end
end

function Mesh:writeTextureFiles()
   local imageExtention = ".ppm"
   if self.makeColorImage then
      self.colorImage:writePPM(self.colorImageName .. imageExtention)
   end
   if self.makeNormalImage then
      self.normalImage:writePPM(self.normalImageName .. imageExtention )
   end
end


local offsets = {
   vec3.new(0, 0, 0),
   vec3.new(1, 0, 0),
   vec3.new(0, 1, 0),
   vec3.new(1, 1, 0),
   vec3.new(0, 0, 1),
   vec3.new(1, 0, 1),
   vec3.new(0, 1, 1),
   vec3.new(1, 1, 1),
}

local function CreateScaledOffsets(scale, offset)
   offset = offset or vec3.new(0,0,0)
   local scaledOffsets = {}
   for i, v in ipairs(offsets) do
      scaledOffsets[i] = (v * scale) + offset
   end
   return scaledOffsets
   
end


local PointGrid = {}
PointGrid.__index = PointGrid

function PointGrid:new(start, size, divisions, SDFunction)
   local count = math.pow(2, divisions) + 1
   local function generateGrid()
      local step = size / (count - 1)
      local index = 1
      local v = start:clone()
      local points = {}
      for z = 1, count do
         v.y = start.y
         for y = 1, count do
            v.x = start.x
            for x = 1, count do
               local point = v:clone()
               points[index] = { 
                  point = point,
                  sdv = SDFunction(point)
               }
               v.x = v.x + step
               index = index + 1
            end
            v.y = v.y + step
         end
         v.z = v.z + step
      end
      return points
   end
   local pg = {
      points = generateGrid(),
      sideCount = count,
      xDelta = 1,
      yDelta = count,
      zDelta = count * count
   }
   setmetatable(pg, self)
   return pg
end

function PointGrid:isIndexValid(index)
   return index >= 1 and index <= #self.points
end

function PointGrid:assertIndexValid(index)
   assert(self:isIndexValid(index), string.format("Index %d is out of range (1, %d) for PointGrid", index, #self.points))
end

function PointGrid:get(index)
   if type(index) == "table" then
      local points = {}
      for i, v in ipairs(index) do
         points[i] = self:get(v)
      end
      return points
   else
      self:assertIndexValid(index)
      return self.points[index]
   end
end

function PointGrid:getIndexBetween(indexI, indexK)
   assert(indexI < indexK, "Expected indexI to be less than indexK")
   local diff = indexK - indexI
   local deltas = {self.zDelta, self.yDelta, self.xDelta}
   local deltaIndex = 3
   for i, v in ipairs(deltas) do
      if diff > v then
         deltaIndex = i
         break
      end
   end
   assert(diff % deltas[deltaIndex] == 0, "Indices are not aligned")
   local coordDiff = diff / deltas[deltaIndex]
   assert(coordDiff >= 2, "Difference is too close to get between")
   local newCoordDiff = coordDiff / 2
   return indexI + (newCoordDiff * deltas[deltaIndex])
end

function PointGrid:getRootCubeIndices()
   local lz = 1
   local hz = (self.sideCount - 1) * self.zDelta + 1
   return {
      lz,
      lz + self.yDelta - 1,
      lz + self.yDelta * (self.sideCount - 1),
      lz + self.zDelta - 1, 
      hz,
      hz + self.yDelta - 1,
      hz + self.yDelta * (self.sideCount - 1),
      hz + self.zDelta - 1, 
   }
end

function PointGrid:getEdgeHash(index1, index2)
   self:assertIndexValid(index1)
   self:assertIndexValid(index2)
   if index1 > index2 then
      local temp = index1
      index1 = index2
      index2 = temp
   end
   local diff = index2 - index1
   local coord = 1
   if diff == self.xDelta then
      coord = 1
   elseif diff == self.yDelta then
      coord = 2
   elseif diff == self.zDelta then
      coord = 3
   else
      assert(false, "Indexes are not next to each other")
   end
   return (index1 - 1) * 3 + coord
end


local OctTreeNode = {}
OctTreeNode.__index = OctTreeNode

function OctTreeNode:new(pointGrid, pointIndices, level)
   level = level or 0
   local function GenerateSDValues()
      local sdvs = {}
      for i, pointIndex in ipairs(pointIndices) do
         local point = pointGrid:get(pointIndex)
         sdvs[i] = point.sdv
      end
      return sdvs
   end
   
   local function ComputeSideLength()
      local p1 = pointGrid:get(pointIndices[1])
      local p2 = pointGrid:get(pointIndices[2])
      return (p2.point - p1.point):len()
   end
   local sdvs = GenerateSDValues()
   local function ComputeCubeIndex()
      local binaryNumber = ""
      for i, v in ipairs(sdvs) do
         if v.distance <= 0 then
            binaryNumber = "1" .. binaryNumber
         else
            binaryNumber = "0" .. binaryNumber
         end
      end
      return tonumber(binaryNumber, 2)
   end
   local qtn = {
      pointIndices = pointIndices,
      pointGrid = pointGrid,
      state = "unknown",
      hasSurface = false,
      sdvs = sdvs,
      cubeIndex = ComputeCubeIndex(),
      sideLength = ComputeSideLength(),
      level = level,
      children = nil
   }
   setmetatable(qtn, self)
   return qtn
end

function OctTreeNode:getPositions()
   local points = {}
   local vs = self.pointGrid:get(self.pointIndices)
   for i, v in ipairs(vs) do
      points[i] = v.point
   end
   return points
end

function OctTreeNode:subdivide()
   self.children = {}
   self.childIndices = {}
   function makeAllPointsIndices()
      local allPointIndices = {
         [1]  = self.pointIndices[1],
         [3]  = self.pointIndices[2],
         [7]  = self.pointIndices[3],
         [9]  = self.pointIndices[4],
         [19] = self.pointIndices[5],
         [21] = self.pointIndices[6],
         [25] = self.pointIndices[7],
         [27] = self.pointIndices[8]
      }
      local function makeMidpoint(target, a, b)
         local subIndex = self.pointGrid:getIndexBetween(allPointIndices[a], allPointIndices[b])
         allPointIndices[target] = subIndex
         -- Save these off to check for error when collapsing
         table.insert(self.childIndices, subIndex)
      end
      -- xs
      makeMidpoint(2,  1,  3)
      makeMidpoint(8,  7,  9)
      makeMidpoint(20, 19, 21)
      makeMidpoint(26, 25, 27)
      
      -- ys
      makeMidpoint(4,  1,  7)
      makeMidpoint(6,  3,  9)
      makeMidpoint(22, 19, 25)
      makeMidpoint(24, 21, 27)
      
      -- zs
      makeMidpoint(10,  1,  19)
      makeMidpoint(12,  3,  21)
      makeMidpoint(16,  7,  25)
      makeMidpoint(18,  9,  27)
      
      -- xys
      makeMidpoint(5,  4,  6)
      makeMidpoint(23, 22, 24)
      
      -- yzs
      makeMidpoint(13, 4, 22)
      makeMidpoint(15, 6, 24)
      
      -- zxs
      makeMidpoint(11, 10, 12)
      makeMidpoint(17, 16, 18)
      
      -- center
      makeMidpoint(14, 13, 15)
      return allPointIndices
   end
   local api = makeAllPointsIndices()
   local pointIndicesList = {
      {api[1],  api[2],  api[4],  api[5],  api[10], api[11], api[13], api[14]},
      {api[2],  api[3],  api[5],  api[6],  api[11], api[12], api[14], api[15]},
      {api[4],  api[5],  api[7],  api[8],  api[13], api[14], api[16], api[17]},
      {api[5],  api[6],  api[8],  api[9],  api[14], api[15], api[17], api[18]},
      {api[10], api[11], api[13], api[14], api[19], api[20], api[22], api[23]},
      {api[11], api[12], api[14], api[15], api[20], api[21], api[23], api[24]},
      {api[13], api[14], api[16], api[17], api[22], api[23], api[25], api[26]},
      {api[14], api[15], api[17], api[18], api[23], api[24], api[26], api[27]},
   }
   for i, pointIndices in ipairs(pointIndicesList) do
      self.children[i] = OctTreeNode:new(self.pointGrid, pointIndices, self.level + 1)
   end
   self.state = "root"
end

function OctTreeNode:computeMaxDistance()
   local function GetPoint(index)
      return self.pointGrid:get(self.pointIndices[index]).point
   end
   local p1 = GetPoint(1)
   local p2 = GetPoint(8)
   local diff = p2 - p1
   return diff:length()
end

function OctTreeNode:estimateSDVAt(samplePoint)
   local function lerp(a, b, p)
      return a * (1 - p) + b * p
   end
   local points = self.pointGrid:get(self.pointIndices)
   local diff = samplePoint - points[1].point
   local percent = vec3.new(diff.x / self.sideLength, diff.y / self.sideLength, diff.z / self.sideLength)
   -- Using Edge number from cube diagram
   local e1  = lerp(points[1].sdv.distance, points[2].sdv.distance, percent.x)
   local e3  = lerp(points[3].sdv.distance, points[4].sdv.distance, percent.x)
   local e5  = lerp(points[5].sdv.distance, points[6].sdv.distance, percent.x)
   local e7  = lerp(points[7].sdv.distance, points[8].sdv.distance, percent.x)
   local e4  = lerp(e1, e3, percent.y)
   local e8  = lerp(e5, e7, percent.y)
   local e12 = lerp(e4, e8, percent.z)
   return e12
end

function OctTreeNode:shouldCollapse()
   if self.state ~= "root" then
      return false
   end
   local allEmpty = true
   local allFull = true
   for i, node in ipairs(self.children) do
      if (node.state == "leaf" or 
          node.state == "root") then
         allEmpty = false
         allFull = false
      elseif node.state == "empty" then
         allFull = false
      else
         allEmpty = false
      end
   end
   if allEmpty or allFull then
      return true
   end
   local maxError = 0
   for i, index in ipairs(self.childIndices) do
      local real = self.pointGrid:get(index)
      local est = self:estimateSDVAt(real.point)
      local err = math.abs(est - real.sdv.distance)
      maxError = math.max(maxError, err)
   end
      
   return maxError < 0.001
end

function OctTreeNode:collapse()
   if self:shouldCollapse() then
      local hasEmpty = false
      local hasFull = false
      local hasLeaf = false
      for i, node in ipairs(self.children) do
         if node.state == "leaf" then
            hasLeaf = true
         elseif node.state == "empty" then
            hasEmpty = true
         else
            hasFull = true
         end
      end
      if hasLeaf then
         self.state = "leaf"
      elseif hasEmpty then
         self.state = "empty"
      else
         self.state = "full"
      end
      self.children = nil
   end
end

function OctTreeNode:build(maxDivisions)
   self.hasSurface = false
   local canDivideMore = maxDivisions > 0
   if canDivideMore then
      self:subdivide()
      for i, child in ipairs(self.children) do
         child:build(maxDivisions - 1)
         if child.hasSurface then
            self.hasSurface = true
         end
      end
      self:collapse()
   else -- canDivideMore is false
      -- Check to see if we contain a surface
      assert(self.cubeIndex >= 0 and self.cubeIndex <= 255, "Cube Index out of range")
      self.hasSurface = (self.cubeIndex ~= 0) and (self.cubeIndex ~= 255)
      if self.cubeIndex == 0 then
         self.state = "empty"
      elseif self.cubeIndex == 255 then
         self.state = "full"
      else
         self.state = "leaf"
      end
   end
end

function OctTreeNode:getBoundingBoxAsString()
   local points = self:getPositions()
   return string.format("%f, %f, %f, %f, %f, %f",
                        points[1].x, points[1].y, points[1].z,
                        points[8].x, points[8].y, points[8].z)
end

function OctTreeNode:__tostring()
   return string.format("OTN(%s, %s)", self.state, self:getBoundingBoxAsString())
end

function OctTreeNode:isLeaf()
   return self.state == "leaf"
end

function OctTreeNode:hasChildren()
   return self.children ~= nil
end


function OctTreeNode:addToSurfaceList(surfaceList)
   if self.hasSurface then
      if self:isLeaf() then
         table.insert(surfaceList, self)
      elseif self:hasChildren() then
         for i, child in ipairs(self.children) do
            child:addToSurfaceList(surfaceList)
         end
      end
   end
end

-- Axes are:
--
--      y
--      |     z
--      |   /
--      | /
--      +----- x
--
-- Vertex and edge layout:
--
--            7             8
--            +-------------+               +-----7-------+   
--          / |           / |             / |            /|   
--        /   |         /   |          12   8         11  6
--    3 +-----+-------+  4  |         +-----+3------+     |   
--      |   5 +-------+-----+ 6       |     +-----5-+-----+   
--      |   /         |   /           4   9         2   10
--      | /           | /             | /           | /       
--    1 +-------------+ 2             +------1------+         

local consts = (function()
   local dir_left_right = 1
   local dir_front_back = 2
   local dir_bottom_top = 3
   local c = {
      dir_left_right = dir_left_right,
      dir_front_back = dir_front_back,
      dir_bottom_top = dir_bottom_top,
   }
   
   c.faceDataFace = {
      [dir_front_back] = {
         {5, 1}, {6, 2}, {7, 3}, {8, 4}
      },
      [dir_left_right] = {
         {2, 1}, {4, 3}, {6, 5}, {8, 7}
      },
      [dir_bottom_top] = {
         {3, 1}, {4, 2}, {7, 5}, {8, 6}
      },
   }
   c.faceDataEdge = {
      [dir_front_back] = {
         {5, 7, 1, 3, 1, dir_left_right},
         {6, 8, 2, 4, 1, dir_left_right},
         {7, 8, 3, 4, 1, dir_bottom_top},
         {5, 6, 1, 2, 1, dir_bottom_top},
      },
      [dir_left_right] = {
         {2, 1, 4, 3, 2, dir_front_back},
         {6, 5, 8, 7, 2, dir_front_back},
         {4, 3, 8, 7, 2, dir_bottom_top},
         {2, 1, 6, 5, 2, dir_bottom_top},
      },
      [dir_bottom_top] = {
         {3, 1, 7, 5, 2, dir_left_right},
         {4, 2, 8, 6, 2, dir_left_right},
         {3, 4, 1, 2, 1, dir_front_back},
         {7, 8, 5, 6, 1, dir_front_back},
      },
   }
   c.faceDataEdgeOrder = {
      {1, 1, 2, 2},
      {1, 2, 1, 2},
   }
   
   c.edgeDataEdge = {
      [dir_front_back] = {
         {4, 3, 2, 1}, {8, 7, 6, 5}
      },
      [dir_left_right] = {
         {7, 5, 3, 1}, {8, 6, 4, 2}
      },
      [dir_bottom_top] = {
         {6, 5, 2, 1}, {8, 7, 4, 3}
      },
   }
   
   c.edgeBaseOnDir = {
      [dir_front_back] = {11, 12, 10, 9},
      [dir_left_right] = {7,  5,  3,  1},
      [dir_bottom_top] = {6,  8,  2,  4}
   }
   c.getFacingDirFromEdge = {
      {1, 2}, -- 1
      {4, 2}, -- 2
      {3, 4}, -- 3
      {3, 1}, -- 4
      {5, 6}, -- 5
      {8, 6}, -- 6
      {7, 8}, -- 7
      {7, 5}, -- 8
      {1, 5}, -- 9
      {2, 6}, -- 10
      {4, 8}, -- 11
      {3, 7}, -- 12
   }
   c.distanceEffect = {
      xs = {0.1, 0.8 },
      ys = {1,   0.1 }
   }
   return c
end)()

function OctTreeNode:cellProc(faceList)
   
   local function areNodesInteresting(ns)
      local allLeafs = true
      local hasUseless = false
      for i, node in ipairs(ns) do
         if not hasUseless and (node.state == "empty" or
                                node.state == "full") then
            hasUseless = true
         end
         if allLeafs and node.state ~= "leaf" then
            allLeafs = false
         end
      end
      return not (hasUseless or allLeafs)
   end
   
   local function edgeProc(ns, dir)
      assert(#ns == 4, "Expected 4 parameters for edgeProc")
      
      if (ns[1]:isLeaf() and ns[2]:isLeaf() and 
          ns[3]:isLeaf() and ns[4]:isLeaf()) then
         local testNodexIndex = 1
         local edgeIndex = consts.edgeBaseOnDir[dir][testNodexIndex]
         local vertPair = consts.getFacingDirFromEdge[edgeIndex]
         local points = { 
            self.pointGrid:get(ns[1].pointIndices[vertPair[1]]),
            self.pointGrid:get(ns[1].pointIndices[vertPair[2]])
         }
         local totalyInside = (points[1].sdv.distance <= 0 and 
                               points[2].sdv.distance <= 0)
         local totalyOutside = (points[1].sdv.distance > 0 and 
                                points[2].sdv.distance > 0)
         if not (totalyInside or totalyOutside)  then
            local flip = points[1].sdv.distance > 0 -- Flip if the inside point is outside
            if flip then
               local temp = ns[2]
               ns[2] = ns[3]
               ns[3] = temp
            end
            -- Rebuild Table without duplicates
            local clean = {}
            local cache = {}
            local removed = 0
            for i, node in ipairs(ns) do
               if cache[node] == nil then
                  cache[node] = true
                  table.insert(clean, node)
               else
                  removed = i
               end
            end
            -- Trial and error to get the triangles to face the correct way
            if removed == 2 then
               local temp = clean[2]
               clean[2] = clean[3]
               clean[3] = temp
            end
            table.insert(faceList, clean)
         end
      elseif areNodesInteresting(ns) then
         for i = 1, 2 do
            local newNs = {}
            local c = consts.edgeDataEdge[dir][i]
            for k, node in ipairs(ns) do
               if node:hasChildren() then
                  newNs[k] = node.children[c[k]]
               else
                  newNs[k] = node
               end
            end
            edgeProc(newNs, dir)
         end
      end
   end
   local function faceProc(ns, dir)
      assert(#ns == 2, "Expected 2 parameters for faceProc")

      if areNodesInteresting(ns) then
         -- sub faceProc
         for i = 1,4 do
            local c = consts.faceDataFace[dir][i]
            local newNs = {}
            for k, node in ipairs(ns) do
               if node:hasChildren() then
                  newNs[k] = node.children[c[k]]
               else
                  newNs[k] = node
               end
            end
            faceProc(newNs, dir)
         end
         -- sub edgeProc
         for i = 1,4 do
            local c = consts.faceDataEdge[dir][i]
            local order = c[5]
            local newDir = c[6]
            local newNs = {}
            for k = 1,4 do
               local node = ns[consts.faceDataEdgeOrder[order][k]]
               if node:hasChildren() then
                  newNs[k] = node.children[c[k]]
               else
                  newNs[k] = node
               end
            end
            edgeProc(newNs, newDir)
         end
         
      end
   end

   local c = consts
   if not self:isLeaf() and self:hasChildren() then
      local cs = self.children
      for i, node in ipairs(cs) do
         node:cellProc(faceList)
      end

      faceProc({cs[1], cs[2]}, c.dir_left_right)
      faceProc({cs[1], cs[3]}, c.dir_bottom_top)
      faceProc({cs[2], cs[4]}, c.dir_bottom_top)
      faceProc({cs[3], cs[4]}, c.dir_left_right)

      faceProc({cs[5], cs[6]}, c.dir_left_right)
      faceProc({cs[5], cs[7]}, c.dir_bottom_top)
      faceProc({cs[6], cs[8]}, c.dir_bottom_top)
      faceProc({cs[7], cs[8]}, c.dir_left_right)
      
      faceProc({cs[1], cs[5]}, c.dir_front_back)
      faceProc({cs[2], cs[6]}, c.dir_front_back)
      faceProc({cs[3], cs[7]}, c.dir_front_back)
      faceProc({cs[4], cs[8]}, c.dir_front_back)

      edgeProc({cs[1], cs[2], cs[3], cs[4]}, c.dir_front_back) -- front
      edgeProc({cs[5], cs[6], cs[7], cs[8]}, c.dir_front_back) -- back
      edgeProc({cs[1], cs[2], cs[5], cs[6]}, c.dir_bottom_top) -- bottom
      edgeProc({cs[3], cs[4], cs[7], cs[8]}, c.dir_bottom_top) -- top
      edgeProc({cs[1], cs[3], cs[5], cs[7]}, c.dir_left_right) -- left
      edgeProc({cs[2], cs[4], cs[6], cs[8]}, c.dir_left_right) -- right
   end
end

function OctTreeNode:getDualPoint()
   if self.point == nil then
      local basePoint = self.pointGrid:get(self.pointIndices[1]).point
      local half = self.sideLength / 2
      local p = basePoint:clone()
      p.x = p.x + half
      p.y = p.y + half
      p.z = p.z + half
      self.point = p
   end
   return self.point
end

function OctTreeNode:getTransitionEdgeList()
   local edgeToVerts = consts.getFacingDirFromEdge
   local transitions = {}
   for edgeIndex = 1, 12 do
      local pointIndices = edgeToVerts[edgeIndex]
      local p1 = self.pointGrid:get(self.pointIndices[pointIndices[1]])
      local p2 = self.pointGrid:get(self.pointIndices[pointIndices[2]])
      
      if (p1.sdv.distance > 0) ~= (p2.sdv.distance > 0) then
         table.insert(transitions, {
            edgeIndex = edgeIndex,
            p1 = p1,
            p2 = p2,
         })
      end
   end 
   return transitions
end

function OctTreeNode:getTransitionEdgeListWithZeroPoint(SDFunction)
   local transitions = self:getTransitionEdgeList()
   for i, t in ipairs(transitions) do
      local from = t.p1.point
      local to   = t.p2.point
      if t.p1.sdv.distance < 0 then
         -- We made a bad guss, flip them around
         from = t.p2.point
         to   = t.p1.point
      end
      t.cast = SDF.castRay(SDFunction, from, to - from)
      --assert(t.cast.hit, "Ray should have hit")
      if t.cast.hit then
         t.zeroPoint = t.cast.point
      else
         t.zeroPoint = from:lerp(to, 0.5)
         print("Warning, ray cast Missed. Expect Bumpy Geometry.")
      end
   end
   return transitions
end

function interpolate(x, points)
   assert(#points.xs == #points.ys, "xs and ys should be the same size")
   local size = #points.xs
   if x <= points.xs[1] then
      return points.ys[1]
   end
   if x >= points.xs[size] then
      return points.ys[size]
   end
   local index = 1
   for i = 2, size do
      if x < points.xs[i] then
         index = i - 1
         break
      end
   end
   local percent = (x - points.xs[index] ) / (points.xs[index + 1] - points.xs[index])
   return points.ys[index] + (points.ys[index + 1] - points.ys[index]) * percent

end

function OctTreeNode:_processVertex(SDFunction)
   local transitions = self:getTransitionEdgeListWithZeroPoint(SDFunction)
   local cubePoints = self:getPositions()
   local point = self:getDualPoint()
   local center = point:clone()
   local halfSizeLength = self.sideLength / 2
   -- Cache Normals
   for i, t in ipairs(transitions) do
      t.normal = SDF.computeNormal(SDFunction, t.zeroPoint)
   end
   local function boundToCube(point)
      local function saturate(v, min, max)
         return math.max(math.min(v, max), min)
      end
      point.x = saturate(point.x, cubePoints[1].x, cubePoints[8].x)
      point.y = saturate(point.y, cubePoints[1].y, cubePoints[8].y)
      point.z = saturate(point.z, cubePoints[1].z, cubePoints[8].z)
   end
   local function moveToPlane(transition)
      local diff = point - transition.zeroPoint
      local dot = diff:dot(transition.normal)
      -- We are going to manipulate the dual point in memory
      -- Add a bit of a scale so the move is gental
      local distanceFactor = dot / halfSizeLength
      local power = interpolate(distanceFactor, consts.distanceEffect)
      point.x = point.x - transition.normal.x * dot * power
      point.y = point.y - transition.normal.y * dot * power
      point.z = point.z - transition.normal.z * dot * power
     
      boundToCube(point) 

   end
   -- I might have just invented this method. 
   -- keep pushing the vertex to the plane created by the edge point and
   -- normal
   -- more iterations should be more accurate?
   local itterations = 40
   for i=1,itterations do
      local prev = point:clone()
      for k, t in ipairs(transitions) do
         moveToPlane(t)
      end
      -- force us back to the cube if we are out of it 
      if (point - prev):len() < 0.0001 then
         --print("settled at:", i)
         break
      end
   end
end

function OctTreeNode:fitVertex(SDFunction)
   if self.state == "root" then
      for i, node in ipairs(self.children) do
         node:fitVertex(SDFunction)
      end
   elseif self.state == "leaf" then
      self:_processVertex(SDFunction)   
   end
   
end

local OctTree = {}
OctTree.__index = OctTree

function OctTree:new(pointGrid, maxDivisions)
   local qt = {
      root = OctTreeNode:new(pointGrid, pointGrid:getRootCubeIndices()),
      maxDivisions = maxDivisions,
      pointGrid = pointGrid
   }
   setmetatable(qt, self)
   --print("Root Node:", qt.root)
   return qt
end

function OctTree:build()
   self.root:build(self.maxDivisions)
end

function OctTree:getSurfaceList()
   local surfaceList = {}
   self.root:addToSurfaceList(surfaceList)
   return surfaceList
end

function OctTree:getFaceList()
   local faceList = {}
   self.root:cellProc(faceList)
   return faceList
end

function OctTree:fittingVertices(SDFunction)
   self.root:fitVertex(SDFunction)
end

function OctTree:getBoundedSurfaceList(lowerBounds, upperBounds)
   local list = self:getSurfaceList()
   local filtered = {}
   for i, node in ipairs(list) do
      local p = node:getPositions()
      if ((p[1].x >= lowerBounds.x and p[1].y >= lowerBounds.y and p[1].z >= lowerBounds.z) and
          (p[8].x <= upperBounds.x and p[8].y <= upperBounds.y and p[8].z <= upperBounds.z)) then
         table.insert(filtered, node)
      end
   end
   return filtered
end

local Modeler = {}

local function vec3getLargestComponent(v)
   local l = v.x
   l = math.max(l, v.y)
   l = math.max(l, v.z)
   return l
end

function Modeler.make(name, SDFunction, maxCubeSize, lowerBounds, upperBounds, textureResolution)
   assert(name ~= nil, "Expected Name")
   assert(SDFunction ~= nil, "Expected SDFunction")
   maxCubeSize = maxCubeSize or 0.5
   upperBounds = upperBounds or vec3.new( 10,  10,  10)
   lowerBounds = lowerBounds or vec3.new(-10, -10, -10)
   textureResolution = textureResolution or 512
   
   local function PrintSettings()
      local f = string.format
      print()
      print(f("Called Modeler.make() at %s", os.date()))
      print(f("   name:              %s", name))
      print(f("   SDFunction:        %s", SDFunction))
      print(f("   maxCubeSize:       %f", maxCubeSize))
      print(f("   lowerBounds:       %s", lowerBounds))
      print(f("   upperBounds:       %s", upperBounds))
      print(f("   textureResolution: %d", textureResolution))
      print()
   end
   PrintSettings()
   
   
   local timerSet = TimerSet:new()
   
   local function StartTask(name)
      timerSet:start(name)
      print()
      print("==== " .. name .. " ====")
   end
   
   StartTask("Initial Sampling")
   
   local spaceData = (function()
      -- Find a cube that encloses the bounds space
      local boundsSize = upperBounds - lowerBounds
      local largestComp = vec3getLargestComponent(boundsSize)
      local fudge = largestComp * 0.01
      local minSize = largestComp + fudge * 2
      local cubesInSize = minSize / maxCubeSize
      local divisions = math.ceil(math.log(cubesInSize) / math.log(2))
      local size = math.pow(2, divisions) * maxCubeSize
      local start = vec3.new(lowerBounds.x - fudge, lowerBounds.y - fudge, lowerBounds.z - fudge)
      return {
         start = start,
         size = size,
         divisions = divisions
      }
   end)()
   local pointGrid = PointGrid:new(spaceData.start, spaceData.size, spaceData.divisions, SDFunction)

   StartTask("Building OctTree")
   local octTree = OctTree:new(pointGrid, spaceData.divisions)

   octTree:build()

   StartTask("Fitting Vertices")
   octTree:fittingVertices(SDFunction)   

   StartTask("Building Faces")
   local faceList = octTree:getFaceList()
   
   StartTask("Assembling Geometry")
   local surfaceList = octTree:getSurfaceList()
   local mesh = Mesh:new(name)
   
   
   for i, nodeList in ipairs(faceList) do
      local points = {}
      for k, node in ipairs(nodeList) do
         points[k] = node:getDualPoint()
      end
      mesh:addTraingle(points[1], points[2], points[3])
      if #points > 3 then
         mesh:addTraingle(points[3], points[2], points[4])
      end
   end
   
   print("Nodes Found:  ", #surfaceList)
   print("Faces Found:  ", #faceList)
   
   
   StartTask("Generating UV Map")
   mesh:generateUVMap(textureResolution)

   StartTask("Writing Model Files")
   mesh:writeObjModelFiles()

   StartTask("Generating Textures")
   mesh:renderMaps(textureResolution, SDFunction)

   StartTask("Writing Texture Files")
   mesh:writeTextureFiles()


   timerSet:stop()
   print()
   print("Timer Report:")
   timerSet:report()
   
end

return Modeler
