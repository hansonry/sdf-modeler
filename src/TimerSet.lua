local TimerSet = {}


TimerSet.__index = TimerSet


function TimerSet:new()
   local ts = {
      nextTimerIndex = 1,
      timerRunning = false,
      list = {}
   }
   setmetatable(ts, self)
   return ts
end


local function ComputeDiff(timer)
   return timer.stop_seconds - timer.start_seconds
end

local function StopTimer(timer, timestamp, printLastStep)
   printLastStep = printLastStep or true
   timer.stop_seconds = timestamp
   if printLastStep then
      local f = string.format
      local diff_seconds = ComputeDiff(timer)
      print(f("%s Took %.3f seconds", timer.name, diff_seconds))
   end
end

function TimerSet:start(name, printLastStep)
   local timestamp = os.clock()
   -- Stop Previous Timer
   local lastTimerIndex = self.nextTimerIndex - 1
   if lastTimerIndex >= 1 and self.timerRunning then
      StopTimer(self.list[lastTimerIndex], timestamp, printLastStep)
   end
   local newTimerIndex = self.nextTimerIndex
   self.nextTimerIndex = self.nextTimerIndex + 1
   self.list[newTimerIndex] = {
      name = name,
      start_seconds = timestamp
   }
   self.timerRunning = true
end


function TimerSet:stop(printLastStep)
   local timestamp = os.clock()
   local lastTimerIndex = self.nextTimerIndex - 1
   assert(lastTimerIndex >= 1 and self.timerRunning, "No timers started")
   StopTimer(self.list[lastTimerIndex], timestamp, printLastStep)
   self.timerRunning = false
end


function TimerSet:report()
   assert(not self.timerRunning, "Timer is still running")
   assert(#self.list > 0, "No recorded times")
   local f = string.format
   local sum_seconds = 0
   local largestNameSize = 0
   for i, timer in ipairs(self.list) do
      local diff_seconds = ComputeDiff(timer)
      sum_seconds = sum_seconds + diff_seconds
      timer.diff_seconds = diff_seconds
      largestNameSize = math.max(largestNameSize, string.len(timer.name))
   end
   
   for i, timer in ipairs(self.list) do
      local percent = timer.diff_seconds / sum_seconds
      local nameSize = string.len(timer.name)
      local nameSizeDiff = largestNameSize - nameSize
      
      local nameField = f("%s:%s", timer.name, string.rep(" ", nameSizeDiff))
      print(f("   %s  %.2fs  %5.1f%%", nameField, timer.diff_seconds, percent * 100))
   end
   print(f("Total Time: %fs", sum_seconds))
end



return TimerSet
