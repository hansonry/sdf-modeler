local Image = {}
Image.__index = Image

function Image:new(width, height, fr, fg, fb)
   local image = {
      width = width,
      height = height,
      data = {}
   }
   setmetatable(image, self)
   image:clear(fr, fg, fb, image.width * image.height * 3)
   return image
end

function Image:clear(r, g, b, count)
   r = r or 0
   g = g or 0
   b = b or 0
   count = count or #self.data
   for i = 1, count, 3 do
      self.data[i] = r
      self.data[i + 1] = g
      self.data[i + 2] = b
   end
end

function Image:areGoodCoords(x, y)
   return (x >= 1 and x <= self.width and
           y >= 1 and y <= self.height)
end

function Image:assertGoodCoords(x, y)
   assert(self:areGoodCoords(x, y), 
          string.format("coordinates out of bounds (%d, %d)", x, y))
end

function Image:getIndexFromCoords(x, y)
   self:assertGoodCoords(x, y)
   return ((x - 1) + (y - 1) * self.width) * 3 + 1
end

function Image:get(x, y)
   local index = self:getIndexFromCoords(x, y)
   return self.data[index], self.data[index + 1], self.data[index + 2]
end

function Image:set(x, y, r, g, b)
   r = r or 0
   g = g or 0
   b = b or 0
   local index = self:getIndexFromCoords(x, y)
   self.data[index] = r
   self.data[index + 1] = g
   self.data[index + 2] = b
end


function Image:writePPM(filename)
   local f = string.format
   local fh = io.open(filename, "w")
   fh:write("P3\n")
   fh:write(f("%d %d\n", self.width, self.height))
   fh:write("255\n")
   for i = 1, #self.data, 3 do
      fh:write(f("%d %d %d\n", math.floor(self.data[i] * 255), 
                               math.floor(self.data[i + 1] * 255), 
                               math.floor(self.data[i + 2] * 255)))
   end
   fh:close()
end

return Image