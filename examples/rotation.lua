local sdf_modeler = require(".")
local vec3 = sdf_modeler.cpml.vec3
local SDF = sdf_modeler.SDF
local Modeler = sdf_modeler.Modeler

function SDFucntion(position)

   local red = {1, 0, 0}
   local blue = {0, 0, 1}
   local green = {0, 1, 0}
   
   local rotPos = SDF.rotateAxisAngle(SDF.translate(position, 0, 1, 0), vec3.new(1, 0, 0), 45 * 3.14/180)

   local sphere = SDF.makeSphere(position, 1.2, red)
   local box = SDF.makeBox(position, vec3.new(1, 1, 1), blue)
   local cylinder = SDF.makeCylinder(rotPos, 1.5, 0.3, green) 
   return sphere:union(box):subtract(cylinder)
end


Modeler.make("boxSphere", SDFucntion,
             0.1, vec3.new(-3, -3, -3), vec3.new(3, 3, 3),
             512)


