local sdf_modeler = require(".")
local vec3 = sdf_modeler.cpml.vec3
local SDF = sdf_modeler.SDF
local Modeler = sdf_modeler.Modeler

local function makePipe(position)
   local yellow = {0.8, 0.8, 0}
   local ouside = SDF.makeCylinder(position, 0.45, .2, yellow) 
   local inside = SDF.makeCylinder(position, 0.5, .18, yellow) 
   return ouside:union(inside)
end



local function SDFucntion(position)

   
   --local rotPos = SDF.rotateAxisAngle(SDF.translate(position, 0, 1, 0), vec3.new(1, 0, 0), 45 * 3.14/180)
   local p1 = SDF.translate(position, 0.5, 0, 0)
   local pyPercent = (position.y + 0.8) / 1.6
   if position.y <= -0.8 then 
      pyPercent = 0 
   elseif position.y >= 0.8 then
      pyPercent = 1 
   end
   --local p2 = SDF.rotateAxisAngle(p1, vec3.new(1, 0, 0), (90 * pyPercent) * 3.14/180)
   --local p2 = SDF.rotateAxisAngle(p1, vec3.new(1, 0, 0), 90 * 3.14/180)
   -- Convert to polar
   local angle = math.atan2(p1.y, p1.x)
   local dist = p1:len()
   angle =  angle + (90 * pyPercent) * 3.14/180
   p1.y = math.sin(angle) * dist
   p1.x = math.sin(angle) * dist
   
   local p3 = SDF.translate(p1, -0.5, 0, 0)
   
   local pipe = makePipe(p3)
   return pipe
end


Modeler.make("pipe", SDFucntion,
             0.01, vec3.new(-0.6, -0.6, -0.6), vec3.new(0.6, 0.6, 0.6),
             512)


