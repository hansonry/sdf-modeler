# SDF Modeler

The intent of this tool is to generated meshes, UVs, and textures for a 3D model that can be used in a game from a Signed Distance Function.


## Example:

```lua
local sdf_modeler = require(".")
local vec3 = sdf_modeler.cpml.vec3
local SDF = sdf_modeler.SDF
local Modeler = sdf_modeler.Modeler

function SDFucntion(position)

   local red = {1, 0, 0}
   local blue = {0, 0, 1}
   local green = {0, 1, 0}

   local sphere = SDF.makeSphere(position, 1.2, red)
   local box = SDF.makeBox(position, vec3.new(1, 1, 1), blue)
   local cylinder = SDF.makeCylinder(position, 1.5, 0.3, green) 
   return sphere:union(box):subtract(cylinder)
end


Modeler.make("boxSphere", SDFucntion,
             0.1, vec3.new(-3, -3, -3), vec3.new(3, 3, 3),
             512)
```

Results in this obj file:
![boxSphere Solid](doc/boxSphereSolid.png)
![boxSphere Wire](doc/boxSphereWire.png)
![boxSphere Bottom](doc/boxSphereBottom.png)

And these textures:
![boxSphere Color](doc/boxSphere_color.png)
![boxSphere Normal](doc/boxSphere_normal.png)

## FAQ

 - Q: SDF?
    - Signed Distance Functions. A way to make geometry using math.
    - [Inigo Quilez does a good job explaining some of this](https://iquilezles.org/articles/distfunctions/)
 - Q: What are the ppm files. 
    - They are all text based image file formats. Use an image editor to convert
      them into pngs.
 - Q: How do I fix the strange artifacting on sharp edges.
    - This is probably happening because the math is working out too well. Try
      moving your minimum bounds by a small amount. That should jog everything.
 - Q: Why lua?
    - I wanted the artist to have full control of the Signed Distance Function.
      Lua is pretty fast for a scripting language and there is no need to
      recompile.
      
## Known Issues

Need more SDF shapes and helper functions like translations and rotations.

The textures currently come out at ppms. Which are not very compressed and
need to be converted. This is a limitation because lua has no native png 
support.

Triangles sometimes bunch up and cause artifacts. I hope in the future
to have an optimization pass on the mesh after it is generated to 
remove these kinds of issues.

Lua is fast for a scripting language and nice for defining SDFs, but it is 
slower than other things. I may rewrite in C and get the GPU involved.


## Resources

Big thanks to these people for putting some of this stuff together. 
Without it, this project wouldn't exist.

* https://iquilezles.org/articles/distfunctions/
* https://www.mattkeeter.com/projects/contours/
* https://www.boristhebrave.com/2018/04/15/dual-contouring-tutorial/
* https://github.com/excessive/cpml

