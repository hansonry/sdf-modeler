
local root_dir = (...) and (...):gsub('%.init$', '') .. "." or ""
local src_dir = root_dir .. "src."

local sdf_modeler = {
   _LICENSE = "sdf-modeler is distributed under the terms of the MIT license. See LICENSE.",
   _URL = "https://gitlab.com/hansonry/sdf-modeler",
   _VERSION = "0.0.1",
   _DESCRIPTION = "sdf-modeler. Used to make 3D models from SDFs for use in games."
}

local files = {
   "Image",
   "Modeler",
   "SDF",
   "TimerSet",
}

for _, file in ipairs(files) do
	sdf_modeler[file] = require(src_dir .. file)
end

sdf_modeler["cpml"] = require(root_dir .. "libs.cpml")

return sdf_modeler
